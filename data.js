var input_data = "BEGIN:VCALENDAR\n" +
"PRODID:-//Google Inc//Google Calendar 70.9054//EN\n" +
"VERSION:2.0\n" +
"CALSCALE:GREGORIAN\n" +
"METHOD:PUBLISH\n" +
"X-WR-CALNAME:m-fukumoto@bpm-gr.co.jp\n" +
"X-WR-TIMEZONE:Asia/Tokyo\n" +
"BEGIN:VTIMEZONE\n" +
"TZID:Asia/Tokyo\n" +
"X-LIC-LOCATION:Asia/Tokyo\n" +
"BEGIN:STANDARD\n" +
"TZOFFSETFROM:+0900\n" +
"TZOFFSETTO:+0900\n" +
"TZNAME:JST\n" +
"DTSTART:19700101T000000\n" +
"END:STANDARD\n" +
"END:VTIMEZONE\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161201T010000Z\n" +
"DTEND:20161201T020000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=t-nagami@bpm-gr.co.jp:mailto:t-nagami@bpm-gr.co.jp\n" +
"UID:nnrfvkti08q0nh6n7l1r3e2kkk@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-mizu\n" +
" kami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=d-\n" +
" katou@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=t-naga\n" +
" mi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:t-nagami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" fukumoto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=a-\n" +
" takagi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:a-takagi@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=j-\n" +
" mitsumura@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:j-mitsumura@bpm-gr.co.jp\n" +
"CREATED:20161121T083004Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161130T140116Z\n" +
"LOCATION:東京都港区芝3-8-2\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【永見・利根川・加藤・水上・彰・三ツ村・福本】　長谷工ご挨拶\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161216T000000Z\n" +
"DTEND:20161216T010000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:iko30hnbq0o36p05afntfbs1f8@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=d-kato\n" +
" u@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=a-\n" +
" takagi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:a-takagi@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" mizukami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=RESOURCE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;X-NUM-GUEST\n" +
" S=0:mailto:bpm-gr.co.jp_2d33343635303834322d343332@resource.calendar.google\n" +
" .com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=t-\n" +
" nagami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:t-nagami@bpm-gr.co.jp\n" +
"CREATED:20161203T105249Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161204T225049Z\n" +
"LOCATION:会議室-明大前\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【永見・加藤・水上・彰・福本・利根川】ｷﾘﾏﾝｼﾞｬﾛ　進捗会議\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161206T010000Z\n" +
"DTEND:20161206T030000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=d-katou@bpm-gr.co.jp:mailto:d-katou@bpm-gr.co.jp\n" +
"UID:boaqo9ai8aa5l9p8ke3q090gh0@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=d-kato\n" +
" u@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"CREATED:20161124T055534Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/4951\n" +
"LAST-MODIFIED:20161205T233512Z\n" +
"LOCATION:文京区本郷4-16-1\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【加藤・福本】ｺｱﾛｰﾄﾞ春日205号室退去立会　 %DL新宿_各務\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161206T083000Z\n" +
"DTEND:20161206T093000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=a-takagi@bpm-gr.co.jp:mailto:a-takagi@bpm-gr.co.jp\n" +
"UID:epbrrlb13pjaed81g6okiun4ss@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=a-taka\n" +
" gi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:a-takagi@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=d-kato\n" +
" u@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161202T082503Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161205T233532Z\n" +
"LOCATION:東京都千代田区富士見2-3-11 ｱｯﾌﾟﾙﾋﾞﾙ5階\n" +
"SEQUENCE:1\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:年末ご挨拶【加藤・彰・福本】MHE5\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161212T020000Z\n" +
"DTEND:20161212T030000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:p4qlr655dedt5up19vqhnlkegg@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=t-\n" +
" nagami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:t-nagami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161208T002842Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161208T002933Z\n" +
"LOCATION:\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【永見・利根川・福本】MHE1年末挨拶\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161208T230000Z\n" +
"DTEND:20161209T000000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:dcvv581h567279m6s207c90p78@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=d-kato\n" +
" u@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=t-\n" +
" nagami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:t-nagami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=RESOURCE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;X-NUM-GUEST\n" +
" S=0:mailto:bpm-gr.co.jp_2d33343635303834322d343332@resource.calendar.google\n" +
" .com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" mizukami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=a-\n" +
" takagi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:a-takagi@bpm-gr.co.jp\n" +
"CREATED:20161203T105019Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161208T072145Z\n" +
"LOCATION:会議室-明大前\n" +
"SEQUENCE:2\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【永見・加藤・水上・彰・福本・利根川】ｷﾘﾏﾝｼﾞｬﾛ　進捗会議\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161216T100000Z\n" +
"DTEND:20161216T140000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:uqlmv080m31msp62t2mam9vqno@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=y-\n" +
" yoshioka@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:y-yoshioka@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"CREATED:20161207T122900Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161208T081658Z\n" +
"LOCATION:\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【利根川・吉岡・福本】MHE1松本さん会食\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161209T040000Z\n" +
"DTEND:20161209T060000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=m-mizukami@bpm-gr.co.jp:mailto:m-mizukami@bpm-gr.co.jp\n" +
"UID:l3aqg5ouqbehlg9ra0bq9avq2s@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-mizu\n" +
" kami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161128T081431Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5144\n" +
"LAST-MODIFIED:20161208T224602Z\n" +
"LOCATION:東京都武蔵野市吉祥寺北町1-28-10\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【水上・福本】ｿﾚｱｰﾄﾞ吉祥寺201　%MHE4_市橋\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161210T000000Z\n" +
"DTEND:20161210T003000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=s-noguchi@bpm-gr.co.jp:mailto:s-noguchi@bpm-gr.co.jp\n" +
"UID:oaad82hmogok8tibo0jdpg1m6s@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=s-nogu\n" +
" chi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:s-noguchi@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" tajima@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-tajima@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161209T120434Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161209T125436Z\n" +
"LOCATION:\n" +
"SEQUENCE:1\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【田島･福本】長谷工の写真報告書ﾚｸﾁｬｰお願いできますか?\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161215T020000Z\n" +
"DTEND:20161215T040000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=t-nagami@bpm-gr.co.jp:mailto:t-nagami@bpm-gr.co.jp\n" +
"UID:83hpk37094mhp2i8j3tj407t6g@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-mizu\n" +
" kami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161214T010033Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161215T020302Z\n" +
"LOCATION:\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【水上】ｼﾃｨﾊｲﾂ上野毛　現地打ち合わせ\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161215T060000Z\n" +
"DTEND:20161215T080000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=m-mizukami@bpm-gr.co.jp:mailto:m-mizukami@bpm-gr.co.jp\n" +
"UID:mrb9rvno8bs2j5ac48jgrfpdic@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-mizu\n" +
" kami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"CREATED:20161128T071421Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5090\n" +
"LAST-MODIFIED:20161215T070424Z\n" +
"LOCATION:東京都国分寺市西町5-10-3\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【水上・福本】ﾍﾞﾙｱﾙｶｰｻ国分寺西町503退去立会　%ﾀﾞｲﾜ_武蔵野\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161224T100000Z\n" +
"DTEND:20161224T110000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:i4rvbum2vifh218cua4h5freug@google.com\n" +
"ATTENDEE;CUTYPE=RESOURCE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;X-NUM-GUEST\n" +
" S=0:mailto:bpm-gr.co.jp_2d33343635303834322d343332@resource.calendar.google\n" +
" .com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=a-\n" +
" takagi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:a-takagi@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=d-kato\n" +
" u@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=t-\n" +
" nagami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:t-nagami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" fukumoto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" mizukami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"CREATED:20161216T062812Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161216T084450Z\n" +
"LOCATION:会議室-明大前\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【ｷﾘﾏﾝｼﾞｬﾛ】進捗会議\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161224T093000Z\n" +
"DTEND:20161224T100000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:1c13uidp048jfurs601044qj6o@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=d-\n" +
" katou@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" mizukami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=a-\n" +
" takagi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:a-takagi@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=s-\n" +
" takagi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:s-takagi@bpm-gr.co.jp\n" +
"CREATED:20161217T102520Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161217T120224Z\n" +
"LOCATION:\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【ｷﾘﾏﾝｼﾞｬﾛ】ﾌﾟﾚMTG Presented byしろう\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161221T030000Z\n" +
"DTEND:20161221T050000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:qhdrfqierfkkmfoq4j94ddj7s4@google.com\n" +
"CREATED:20161220T034736Z\n" +
"DESCRIPTION:MHE1野崎さんから電話で確定依頼　再度登録しました\nhttps://my.redmine.jp/bpm-gr/issues\n" +
" /5399\n" +
"LAST-MODIFIED:20161220T093646Z\n" +
"LOCATION:東京都品川区中延7-7-21\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【福本】柿の木ﾊｲﾑ302　退去立会　%MHE1_\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161221T053000Z\n" +
"DTEND:20161221T063000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:dbu6ebu7cipnms8b42letvkr2g@google.com\n" +
"CREATED:20161220T051256Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5392\n駐輪場に使われていない鍵があるので切って欲\n" +
" しい。\n" +
"LAST-MODIFIED:20161220T094532Z\n" +
"LOCATION: 港区三田4-17-36\n" +
"SEQUENCE:1\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【福本】ｱｳﾛｰﾗ三田　駐輪場鍵切断\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161229T000000Z\n" +
"DTEND:20161229T030000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=s-noguchi@bpm-gr.co.jp:mailto:s-noguchi@bpm-gr.co.jp\n" +
"UID:tod1545g3brr1qfsd5vrcvdkj4@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=a-\n" +
" takagi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:a-takagi@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" mizukami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=s-nogu\n" +
" chi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:s-noguchi@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161220T234016Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161221T000033Z\n" +
"LOCATION:\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【水上･ｱｷﾗ･福本】倉庫棚卸し\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161222T023000Z\n" +
"DTEND:20161222T043000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:mai7b8fkjifpl1h5gn1b2mmcsg@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=r-\n" +
" tonegawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161108T053756Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/4949\n" +
"LAST-MODIFIED:20161221T154052Z\n" +
"LOCATION:世田谷区玉川台1-2-13\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【利根川・福本】ﾄﾞｴﾙ瀬田304号室　退去立会　%HLN_\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161226T030000Z\n" +
"DTEND:20161226T050000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:gv2f8ubvtlppl0tcm71t2omr28@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"CREATED:20161202T012255Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5197\n" +
"LAST-MODIFIED:20161222T154503Z\n" +
"LOCATION:東京都港区白金1-10-10\n" +
"SEQUENCE:1\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【利根川・福本】ﾊﾟｰｸﾘｭｸｽ白金mono602　退去立会　%MHE1_松本\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20170106T000000Z\n" +
"DTEND:20170106T010000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:5jo3vtb1r4df3b5csb81ctukgs@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=a-taka\n" +
" gi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:a-takagi@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" mizukami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=d-kato\n" +
" u@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=t-\n" +
" nagami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:t-nagami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=RESOURCE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;X-NUM-GUEST\n" +
" S=0:mailto:bpm-gr.co.jp_2d33343635303834322d343332@resource.calendar.google\n" +
" .com\n" +
"CREATED:20161224T115806Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161225T001534Z\n" +
"LOCATION:会議室-明大前\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【ｷﾘﾏﾝｼﾞｬﾛ】進捗会議\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161226T100000Z\n" +
"DTEND:20161226T130000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=y-yoshioka@bpm-gr.co.jp:mailto:y-yoshioka@bpm-gr.co.jp\n" +
"UID:4lonaddvn85v4uspuk9gjidtho@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;X-NUM\n" +
" -GUESTS=0:mailto:all@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=y-yosh\n" +
" ioka@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:y-yoshioka@bpm-gr.co.jp\n" +
"CREATED:20161219T060828Z\n" +
"DESCRIPTION:19：30～予約ですが　19時以降なら大丈夫\n\n" +
"LAST-MODIFIED:20161225T003228Z\n" +
"LOCATION:東京都 渋谷区神山町5-3\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【all】アヤパニ\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161227T020000Z\n" +
"DTEND:20161227T040000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:fdot2v026gm2hjkom3iksudfi4@google.com\n" +
"CREATED:20161129T053543Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5161\n" +
"LAST-MODIFIED:20161225T215928Z\n" +
"LOCATION:東京都世田谷区松原3-38-15\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【福本】TOP明大前第3　407号室　退去立会　%HLN2\n" +
"TRANSP:OPAQUE\n" +
"X-APPLE-STRUCTURED-LOCATION;VALUE=URI;X-APPLE-MAPKIT-HANDLE=CAES5AEaEgmwLU1\n" +
" bbdVBQBEwDP24onRhQCJ0Cgbml6XmnKwSAkpQGgnmnbHkuqzpg70yDOS4lueUsOiwt+WMujoIMT\n" +
" U2LTAwNDNCBuadvuWOn1IN5p2+5Y6fM+S4geebrloKMzjnlaoxNeWPt2IX5p2+5Y6fM+S4geebr\n" +
" jM455WqMTXlj7eKAQbmnb7ljp8qF+advuWOnzPkuIHnm64zOOeVqjE15Y+3MgvjgJIxNTYtMDA0\n" +
" MzIV5p2x5Lqs6YO95LiW55Sw6LC35Yy6Mhfmnb7ljp8z5LiB55uuMzjnlaoxNeWPtzg5QAA=;X-\n" +
" APPLE-RADIUS=49.91303313349389;X-APPLE-REFERENCEFRAME=1;X-TITLE=\"東京都世田谷区松原3\n" +
" -38-15\":geo:35.667400,139.644864\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161226T050000Z\n" +
"DTEND:20161226T060000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:gr49ois5oi30soaqte192do88c@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=r-\n" +
" tonegawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" fukumoto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161219T010556Z\n" +
"DESCRIPTION:鍵事務所下　KBあり\n" +
"LAST-MODIFIED:20161225T235626Z\n" +
"LOCATION:港区高輪2-4-11\n" +
"SEQUENCE:5\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:鍵持出完【利根川・福本】ﾌｧﾐｰﾙ高輪ｱﾝｼｪｰﾙ306号室　東急　\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161226T073000Z\n" +
"DTEND:20161226T100000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER:mailto:bpm-gr.co.jp_hri3g8dd694vfo4jh7idjt04o0@group.calendar.goo\n" +
" gle.com\n" +
"UID:sjqs3b3kqsqoc3l6illmk73n7c@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;X-NUM\n" +
" -GUESTS=0:mailto:all@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161208T024358Z\n" +
"DESCRIPTION:国立ｵﾘﾝﾋﾟｯｸ記念青少年総合ｾﾝﾀ－ センター棟 302号室\n" +
"LAST-MODIFIED:20161226T071225Z\n" +
"LOCATION:東京都渋谷区代々木神園町３−1\n" +
"SEQUENCE:2\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【全員】事業戦略会議\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161229T040000Z\n" +
"DTEND:20161229T060000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:7v739s9f3nsssrri4soni04p9k@google.com\n" +
"CREATED:20161206T022547Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5209\n" +
"LAST-MODIFIED:20161226T082617Z\n" +
"LOCATION:港区麻布十番2-1-1\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【福本】KINOKUNIYA.S.R702　退去立会　%MHE1_松本\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20170105T060000Z\n" +
"DTEND:20170105T080000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:qfe7ue8qob23nrtta84u7l37oo@google.com\n" +
"CREATED:20161226T001943Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5451\n" +
"LAST-MODIFIED:20161227T042847Z\n" +
"LOCATION:\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:（資料なし）立【福本】ｼﾙﾌｨｰﾄﾞ東品川106　退去立会　%MHE1_松本\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161227T110000Z\n" +
"DTEND:20161227T130000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=d-katou@bpm-gr.co.jp:mailto:d-katou@bpm-gr.co.jp\n" +
"UID:d6p5u04v6rmgeefe8f9vrir1oo@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" mizukami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-mizukami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=r-\n" +
" tonegawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=d-kato\n" +
" u@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=a-\n" +
" takagi@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:a-takagi@bpm-gr.co.jp\n" +
"CREATED:20161224T112643Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20161227T061723Z\n" +
"LOCATION:\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:【ｷﾘﾏﾝｼﾞｬﾛ】ｺﾐｯﾄﾒﾝﾄ問題解決会議\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161228T060000Z\n" +
"DTEND:20161228T080000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:ug161aah6faot3aafp58s2nbhc@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"CREATED:20161209T005009Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5208\n" +
"LAST-MODIFIED:20161227T074619Z\n" +
"LOCATION:港区芝浦4-17-7\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【利根川・福本】ﾌｫﾝﾃｰﾇ芝浦1001　退去立会　　%MHE1_松本\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161229T010000Z\n" +
"DTEND:20161229T030000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:nvum5pm5ksrihku0bbcn8d2dq0@google.com\n" +
"CREATED:20161228T073841Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5204\n" +
"LAST-MODIFIED:20161228T073841Z\n" +
"LOCATION:東京都品川区中延7-7-21\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【福本】柿の木ﾊｲﾑ301　退去立会　%MHE1_松本\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20161228T040000Z\n" +
"DTEND:20161228T060000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=r-tonegawa@bpm-gr.co.jp:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"UID:6u1hoamm721ol8dk3qo2dr99i0@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=r-tone\n" +
" gawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=m-fuku\n" +
" moto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"CREATED:20161202T031902Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5204\n" +
"LAST-MODIFIED:20161228T073922Z\n" +
"LOCATION:東京都品川区中延7-7-21\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:ﾘｽｹ立【福本】柿の木ﾊｲﾑ301　退去立会　%MHE1_松本\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20170106T070000Z\n" +
"DTEND:20170106T090000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:365ioc29k9n42r9g356nijbadg@google.com\n" +
"CREATED:20161222T062009Z\n" +
"DESCRIPTION:MHEPM木村さんから電話で依頼\nhttps://my.redmine.jp/bpm-gr/issues/5455\n\n" +
"LAST-MODIFIED:20161228T115310Z\n" +
"LOCATION:東京都渋谷区松濤1-28-14\n" +
"SEQUENCE:2\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【福本】ｱﾙｽ渋谷松濤402号室　%MHEPM_木村\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20170106T050000Z\n" +
"DTEND:20170106T060000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:158epuetrt97i0oesgtg2cr86s@google.com\n" +
"CREATED:20161227T141310Z\n" +
"DESCRIPTION:\n" +
"LAST-MODIFIED:20170105T060945Z\n" +
"LOCATION:港区白金3-3-9\n" +
"SEQUENCE:5\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:PM完【福本】ﾗ・ｸﾞﾗﾝﾄﾞｩｰﾙ白金103号室　ｺﾝﾛ部材当日\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20170114T020000Z\n" +
"DTEND:20170114T040000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:lgjh0hnhf73e9oauu1t4c71uqg@google.com\n" +
"CREATED:20170106T033916Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5526\n" +
"LAST-MODIFIED:20170106T034102Z\n" +
"LOCATION:品川区大井4-25-20\n" +
"SEQUENCE:0\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【福本】ｽﾌﾟﾚｯｸｽ104号室　退去立会　%MHE1_松本\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20170107T010000Z\n" +
"DTEND:20170107T030000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"UID:k0qevsl23g22ubqaft89i8qr50@google.com\n" +
"CREATED:20161221T000802Z\n" +
"DESCRIPTION:https://my.redmine.jp/bpm-gr/issues/5412\n" +
"LAST-MODIFIED:20170106T085942Z\n" +
"LOCATION:中央区日本橋小伝馬町9-13\n" +
"SEQUENCE:1\n" +
"STATUS:CONFIRMED\n" +
"SUMMARY:立【福本】智光ﾋﾞﾙ401　%MHE5_蛭川\n" +
"TRANSP:OPAQUE\n" +
"END:VEVENT\n" +
"BEGIN:VEVENT\n" +
"DTSTART:20170110T000000Z\n" +
"DTEND:20170110T010000Z\n" +
"DTSTAMP:20170510T131917Z\n" +
"ORGANIZER;CN=d-katou@bpm-gr.co.jp:mailto:d-katou@bpm-gr.co.jp\n" +
"UID:d0ttc1ncmsttkfn0ou7027ee50@google.com\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=t-\n" +
" nagami@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:t-nagami@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=d-kato\n" +
" u@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:d-katou@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=r-\n" +
" tonegawa@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:r-tonegawa@bpm-gr.co.jp\n" +
"ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=m-\n" +
" fukumoto@bpm-gr.co.jp;X-NUM-GUESTS=0:mailto:m-fukumoto@bpm-gr.co.jp\n" +
"END:VCALENDAR\n";
